package projects

import (
	"errors"
	"github.com/jinzhu/gorm"
	"gitlab.com/Alvoras/goportfolio/internal/utils"
	"io/ioutil"
	"path/filepath"
	"reflect"
	"sort"
)

type CategoryContent struct {
	Quantity     int
	Names        []string
	Themes       []string
	Types        []string
	Descriptions []string
	Years        []int64
}

type Project struct {
	gorm.Model
	Name  string `gorm:"column:name;type:varchar(100);primary_key"`
	Type  string `gorm:"column:type;type:varchar(10)"`
	Theme string `gorm:"column:theme;type:varchar(30)"`
	Desc  string `gorm:"column:description;type:varchar(200)"`
	Year  int64  `gorm:"column:year;type:int"`
}

type Projects struct {
	All     ProjectList
	ByType  MappedProjectList
	ByTheme MappedProjectList
}

type ProjectList struct {
	Projects []*Project
	Quantity int
}

type MappedProjectList struct {
	Projects    map[string][]*Project
	SortedIndex []string
	Quantity    int
}

func (mpl *MappedProjectList) GenerateSortedIndex() {
	var keys []string

	bufKeys := reflect.ValueOf(mpl.Projects).MapKeys()
	for _, key := range bufKeys {
		keys = append(keys, key.Interface().(string))
	}
	sort.Strings(keys)

	for _, key := range keys {
		mpl.SortedIndex = append(mpl.SortedIndex, key)
	}
}

func (pj *Project) New(db *gorm.DB) {
	db.NewRecord(pj)
	db.Create(&pj)
}

func (pl *Projects) Load(db *gorm.DB) {
	pl.loadFromDB(db)
	pl.loadByType(db)
	pl.loadByTheme(db)
	pl.ByTheme.GenerateSortedIndex()
	pl.ByType.GenerateSortedIndex()
}

func (pl *Projects) loadFromDB(db *gorm.DB) {
	var pj []*Project

	if errors := db.Select("name, type, theme, description, year").Find(&pj).GetErrors(); errors != nil {
		for _, err := range errors {
			utils.HandleErr(err, true)
		}
	}

	for _, p := range pj {
		pl.All.Projects = append(pl.All.Projects, p)
	}
	pl.All.Quantity = len(pl.All.Projects)
}

func (pl *Projects) loadByType(db *gorm.DB) {
	pl.ByType.Projects = make(map[string][]*Project)
	for _, p := range pl.All.Projects {
		pl.ByType.Projects[p.Type] = append(pl.ByType.Projects[p.Type], p)
	}
	pl.ByType.Quantity = len(pl.ByType.Projects)
}

func (pl *Projects) loadByTheme(db *gorm.DB) {
	pl.ByTheme.Projects = make(map[string][]*Project)
	for _, p := range pl.All.Projects {
		pl.ByTheme.Projects[p.Theme] = append(pl.ByTheme.Projects[p.Theme], p)
	}
	pl.ByTheme.Quantity = len(pl.ByTheme.Projects)
}

func FetchReadme(name string) (error, string) {
	projects := utils.ListDirs("projects")
	ok := false
	var err error

	for _, dirname := range projects {
		if name == dirname {
			ok = true
			break
		}
	}

	if !ok {
		err = errors.New("Project's directory not found")
		return err, ""
	}

	abspath, err := filepath.Abs("projects/" + name + "/README.md")
	utils.HandleErr(err, false)

	content, err := ioutil.ReadFile(abspath)
	utils.HandleErr(err, false)

	return err, string(content)
}

func ListProjects(toFind string, db *gorm.DB) CategoryContent {
	var list CategoryContent
	var projects []*Project

	if errors := db.Select("name, type, theme, description, year").Find(&projects, "type = ? OR theme = ?", toFind, toFind).GetErrors(); errors != nil {
		for _, err := range errors {
			utils.HandleErr(err, true)
		}
	}

	list.Quantity = len(projects)

	for _, p := range projects {
		list.Names = append(list.Names, p.Name)
		list.Types = append(list.Types, p.Type)
		list.Themes = append(list.Themes, p.Theme)
		list.Descriptions = append(list.Descriptions, p.Desc)
		list.Years = append(list.Years, p.Year)
	}

	return list
}

func (pl *Projects) LoadNamesOnly(db *gorm.DB) {
	var pj []*Project

	if errors := db.Select("name").Find(&pj).GetErrors(); errors != nil {
		for _, err := range errors {
			utils.HandleErr(err, true)
		}
	}

	for _, p := range pj {
		pl.All.Projects = append(pl.All.Projects, p)
	}
	pl.All.Quantity = len(pl.All.Projects)
}
