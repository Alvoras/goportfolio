package utils

import (
	log "gitlab.com/Alvoras/goportfolio/internal/loggers"
	"io/ioutil"
	"path/filepath"
)

func HandleErr(err error, doPanic bool) {
	if err != nil {
		if doPanic {
			panic(err)
		} else {
			log.Verbose.Println(err)
		}
	}
}

func ListDirs(path string) []string {
	var dirs []string
	pjPath, err := filepath.Abs(path)
	HandleErr(err, false)

	files, err := ioutil.ReadDir(pjPath)
	HandleErr(err, false)

	for _, file := range files {
		dirs = append(dirs, file.Name())
	}

	return dirs
}
