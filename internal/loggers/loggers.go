package loggers

import (
	"log"
)

var (
	// Event is the logger to log event
	Event *log.Logger
	// Info is the logger to log info
	Info *log.Logger
	// Error is the logger to log errors
	Error *log.Logger
	// Verbose is the logger to log additional info
	Verbose *log.Logger
)
