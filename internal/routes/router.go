package routes

import (
	"encoding/json"
	"github.com/googollee/go-socket.io"
	"github.com/gorilla/mux"
	db "gitlab.com/Alvoras/goportfolio/internal/database"
	pj "gitlab.com/Alvoras/goportfolio/internal/projects"
	"gitlab.com/Alvoras/goportfolio/internal/utils"
	"net/http"
	"net/url"
)

var (
	err    error
	Router *mux.Router
	socket *socketio.Server
)

func init() {
	Router = mux.NewRouter()
	socket, err = socketio.NewServer(nil)
	utils.HandleErr(err, true)

	socket.On("fetchReadme", func(s socketio.Socket, pjName string) {
		decoded, err := url.PathUnescape(pjName)

		err, content := pj.FetchReadme(decoded)
		utils.HandleErr(err, false)

		s.Emit("readmeContent", content)
	})

	socket.On("fetchProjectList", func(s socketio.Socket, pjType string, mode string) {
		decoded, err := url.PathUnescape(pjType)

		list := pj.ListProjects(decoded, db.DB)

		utils.HandleErr(err, false)

		fmtList, err := json.Marshal(&list)
		utils.HandleErr(err, false)

		s.Emit("listProjects", string(fmtList), mode)
	})

	Router.Handle("/socket.io/", socket)
	Router.HandleFunc("/", Index).Methods("GET", "HEAD")
	Router.HandleFunc("/cv", ServeCV)
	Router.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("public"))))
}
