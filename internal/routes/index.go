package routes

import (
	db "gitlab.com/Alvoras/goportfolio/internal/database"
	pj "gitlab.com/Alvoras/goportfolio/internal/projects"
	"html/template"
	"log"
	"net/http"
)

type Page struct {
	Title           string
	ProjectsData    pj.Projects
	FirstListInType []*pj.Project
}

func Index(w http.ResponseWriter, r *http.Request) {
	page := Page{
		Title: "Portfolio",
	}

	page.ProjectsData.Load(db.DB)

	page.FirstListInType = page.ProjectsData.ByType.Projects[page.ProjectsData.ByType.SortedIndex[0]]

	tmpl := template.New("index").Funcs(
		map[string]interface{}{
			"GetFirstInType": func(p Page) *pj.Project {
				return p.FirstListInType[0]
			},
		})

	tmpl = template.Must(
		tmpl.ParseFiles(
			"views/index.tmpl",
			"views/inc/global/head.tmpl",
			"views/inc/global/footer.tmpl",
			"views/inc/global/js-lib.tmpl",
			"views/inc/project/project-content.tmpl",
			"views/inc/project/type-list.tmpl",
			"views/inc/project/project-list.tmpl",
		),
	)

	if err := tmpl.ExecuteTemplate(w, "index", page); err != nil {
		log.Fatal("Failed to execute the template :", err)
	}
}
