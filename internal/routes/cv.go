package routes

import (
	"gitlab.com/Alvoras/goportfolio/internal/utils"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

func ServeCV(w http.ResponseWriter, r *http.Request) {
	cvPath, err := filepath.Abs("public/cv/CV_HadrienBouffier-2018.pdf")
	utils.HandleErr(err, false)

	file, err := os.Open(cvPath)
	utils.HandleErr(err, false)
	defer file.Close()

	w.Header().Set("Content-Disposition", "attachment; filename=\"CV_HadrienBouffier.pdf\"")

	http.ServeContent(w, r, "CV_HadrienBouffier.pdf", time.Now(), file)
}
