package database

import (
	"github.com/jinzhu/gorm"
	log "gitlab.com/Alvoras/goportfolio/internal/loggers"
	"os"
)

var (
	DB *gorm.DB
)

func init() {
	var err error
	DB, err = gorm.Open("sqlite3", "./projects.db")
	if err != nil {
		log.Error.Println("Failed to open the database")
		os.Exit(0)
	}
}
