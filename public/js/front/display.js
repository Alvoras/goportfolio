function displayReadme(readmeContent) {
	let params = {
		html: true,
		breaks: false,
		typographer: true
	};

	let md = window.markdownit(params);

	let mdReadme = md.render(readmeContent);

	$("#readme").html(mdReadme);
}

function resetReadmePanel() {
	$("#readme").html("Aucun projet selectionné");
}

function removeSelectedButton() {
	$(".selected-button").removeClass("selected-button");
}

function removeSelectedType() {
	$(".selected-type").removeClass("selected-type");
}

function removeSelectedProject() {
	$(".selected-project").removeClass("selected-project");
}

function setProjectListTile(name) {
	$("#chosen-project-name").html(name);
}

function displayProjectList(list, mode) {
	JSONList = JSON.parse(list)
	let i = 0;
	let tiles = [];
	let currentTile = "";

	let caption;

	for (i = 0; i < JSONList.Quantity; i++) {
		caption = (mode === "theme")?JSONList.Types[i]:JSONList.Themes[i];
		currentTile = '<div class="project-container" data-project="' + JSONList.Names[i] + '">\
                        <div class="project-content">\
                            <div class="project-header flex-center-row">\
                                <h3 class="project-name flex-center-row">' + JSONList.Names[i] + ' <span class="project-theme">' + caption + '</span></h3>\
                                <h6 class="project-date">' + JSONList.Years[i] + '</h6>\
                            </div>\
                            <h5 class="project-description">' + JSONList.Descriptions[i] + '</h5>\
                        </div>\
                    </div>';

		tiles.push(currentTile);
	}

  $("#projects-list").html(tiles);
}
