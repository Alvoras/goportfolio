package main

import (
	"crypto/tls"
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/kabukky/httpscerts"
	. "gitlab.com/Alvoras/goportfolio/internal/config"
	r "gitlab.com/Alvoras/goportfolio/internal/routes"
	"gitlab.com/Alvoras/goportfolio/internal/utils"
	"log"
	"net/http"
	"time"
)

type AppState struct {
	Env     string
	DoHTTPS bool
}

var (
	appState    AppState
	tlsCfg      *tls.Config
	certPath    string
	keyPath     string
	wrTimeout   time.Duration
	rdTimeout   time.Duration
	idleTimeout time.Duration
)

func init() {
	var err error

	appState = AppState{
		Env:     "dev",
		DoHTTPS: true,
	}

	if appState.DoHTTPS {
		if appState.Env == "dev" {
			certPath = "cert.pem"
			keyPath = "key.pem"

			// Check if the cert files are available
			err = httpscerts.Check("cert.pem", "key.pem")
			// If they are not available, generate new ones
			if err != nil {
				err = httpscerts.Generate("cert.pem", "key.pem", "127.0.0.1:8081")
				if err != nil {
					log.Fatal("Error: Couldn't create https certs.")
				}
			}
		} else {
			certPath = "/etc/letsencrypt/live/hbouffier.info/fullchain.pem"
			keyPath = "/etc/letsencrypt/live/hbouffier.info/privkey.pem"
		}

		tlsCfg = &tls.Config{
			MinVersion:               tls.VersionTLS12,
			CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
			PreferServerCipherSuites: true,
			CipherSuites: []uint16{
				tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
				tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
				tls.TLS_RSA_WITH_AES_256_CBC_SHA,
			},
		}
		wrTimeout, err = time.ParseDuration(Cfg.Timeout.Write)
		utils.HandleErr(err, true)

		rdTimeout, err = time.ParseDuration(Cfg.Timeout.Read)
		utils.HandleErr(err, true)

		idleTimeout, err = time.ParseDuration(Cfg.Timeout.Idle)
		utils.HandleErr(err, true)
	}
}

func main() {
	httpsSrv := &http.Server{
		Addr: fmt.Sprintf("0.0.0.0:%d", Cfg.Server.HTTPS.Port),
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: wrTimeout,
		ReadTimeout:  rdTimeout,
		IdleTimeout:  idleTimeout,
		Handler:      r.Router,
		TLSConfig:    tlsCfg,
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler), 0),
	}

	log.Fatal(httpsSrv.ListenAndServeTLS(certPath, keyPath))
}
