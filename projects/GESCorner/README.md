# GESCorner
------
GESCorner est un site de commande en ligne pour une cafétéria. Le fonctionnement est simple : une fois la commande effectuée, le client doit la valider sur le lieu de retrait de la commande grâce à un QrCode. Une fois validée, la commande apparaîtra sur le Point of Sale du personnel et sera préparée.   

### Démonstration
------
<video controls>
  <source src="/public/videos/GESCorner/gescorner_demo.webm" type="video/webm">
  Your browser does not support HTML5 video.
</video>
