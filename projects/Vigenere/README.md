# Vigenere
------
Chiffre et déchiffre des messages en utilisant le chiffre de Vigenere.
>
Usage : ./vigenere [-k <key>][-t <text>][-cd]

**-c** : Chiffrer

**-d** : Déchiffrer
>
### Démonstration
------
<script src="https://asciinema.org/a/NeiAEXb0Ghk8098hKUNvFBM4Y.js" id="asciicast-NeiAEXb0Ghk8098hKUNvFBM4Y" async></script>
