# Kinodex
------
Présentation
Entièrement cross-plateform, Kinodex est une application codée en NodeJS permettant de maintenir une collection de films à voir ou déjà vus.
>
Bien que programmée entièrement grâce aux technologies du web (HTML/CSS/Javascript/NodeJS), le framework Electron permet de la lancer en tant qu'application de bureau classique.
>
La recherche de film se fait par une requête à une API IMDb.
>
### Démonstration
------
<video controls>
  <source src="/public/videos/Kinodex/kinodex_demo.webm" type="video/webm">
  Your browser does not support HTML5 video.
</video>
