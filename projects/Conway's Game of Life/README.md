# Game of Life
------
Implémentation en C de l'automate cellulaire créé par le mathématicien John Conway en 1970.
>
Il prend en argument au lancement un template qu'il utilisera comme position de départ.
 >
### Démonstration
------
<script src="https://asciinema.org/a/irP2KhCutzQ4eG3jSch6TBARA.js" id="asciicast-irP2KhCutzQ4eG3jSch6TBARA" async></script>
