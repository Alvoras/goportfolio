# MorseMe
------
Codée en C, MorseMe est un utilitaire très léger permettant de transformer la chaîne de caractère entrée en argument du programme en message morse affiché grâce à la LED de la touche verrouillage majuscule.
>
La version Windows utilise l'api système Win32 et la version Linux utilise l'utilitaire xdotool pour simuler les entrées clavier.
