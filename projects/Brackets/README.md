# Brackets
------
Brackets est un forum d'entraide créé pour les étudiants de l'ESGI. Forum classique de question/réponse, il intègre un système de points permettant de récompenser les élèves en fonction du score à la fin du semestre.
>
Codée en NodeJS, la page est actualisé pour tous de manière dynamique. Ainsi, lorsque quelqu'un poste un nouveau thread, il apparaîtra pour les personnes étant sur la page concernée. Il en va de même pour les messages à l'intérieur des threads. Pareillement, les votes sont actualisés en temps réel.
>
### Démonstration
------
<video controls>
  <source src="/public/videos/Brackets/brackets_demo.webm" type="video/webm">
  Your browser does not support HTML5 video.
</video>
