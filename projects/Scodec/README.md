# Scodec
------
### Compatibilité
Linux

### Description
Utilisant un front-end codé avec des technologies web (html/css/javascript/jquery) et affichée avec le framework Electron, l'application est un simple système de codec permettant de chiffrer et de déchiffrer de manière symétrique n'importe quel fichier à partir d'une matrice carrée d'ordre 4.

Exemple de clef : G4C=[10001111 11000111 10100100 10010010]

### Performances
Avec :
- Processeur
    - `cat /proc/cpuinfo|grep 'model name'|head -n1`
        - [Intel(R) Core(TM) i5-2500K CPU @ 3.30GHz](https://ark.intel.com/products/52210/Intel-Core-i5-2500K-Processor-6M-Cache-up-to-3_70-GHz)
- 8Go RAM
- Disque SSD
- Jusqu'à 16 threads

#### Chiffrement
- 50MiB : 0.6616 seconds

- 1000MiB (1GB) : 12.8094 seconds

#### Déchiffrement
- 100MiB : 0.6650 seconds

- 2000MiB (2GB) : 13.0990 seconds

Fichiers générés avec `dd`.

>
### Démonstration
------
<video controls>
  <source src="/public/videos/Scodec/scodec_demo.webm" type="video/webm">
  Your browser does not support HTML5 video.
</video>
